from django import forms
from . import models

class DevForm(forms.Form):
    nome_completo = forms.CharField(max_length=100)
    cidade = forms.CharField(max_length=50)
    telefone = forms.CharField(max_length=20)
    modelo_de_presenca = forms.ModelMultipleChoiceField(queryset=models.ModeloDePresenca.objects.all(), widget=forms.CheckboxSelectMultiple(), help_text='Selecione pelo menos um.')
    tipo_de_trabalho = forms.ModelMultipleChoiceField(queryset=models.TipoDeTrabalho.objects.all(), widget=forms.CheckboxSelectMultiple(), help_text='Selecione pelo menos um.')
    tipo_de_vaga = forms.ModelMultipleChoiceField(queryset=models.TipoDeVaga.objects.all(), widget=forms.CheckboxSelectMultiple(), help_text='Selecione pelo menos um.')
    skills = forms.ModelMultipleChoiceField(queryset=models.Skill.objects.all(), widget=forms.CheckboxSelectMultiple(), help_text='Selecione pelo menos um.')


class DeveloperForm(forms.ModelForm):
    class Meta:
        model = models.Dev
        fields = '__all__'
        exclude = ('usuario',)

        widgets = {
            'modelo_de_presenca': forms.CheckboxSelectMultiple,
            'tipo_de_trabalho': forms.CheckboxSelectMultiple,
            'tipo_de_vaga': forms.CheckboxSelectMultiple,
            'skills': forms.CheckboxSelectMultiple,
        }