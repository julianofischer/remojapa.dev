from random import choices
from django.db import models
from django.contrib.auth.models import User


class ModeloDePresenca(models.Model):
    # presencial, remoto ou híbrido
    nome = models.CharField(max_length=20)

    def __str__(self) -> str:
        return self.nome


class TipoDeTrabalho(models.Model):
    # clt ou pj
    nome = models.CharField(max_length=20)

    def __str__(self) -> str:
        return self.nome

class TipoDeVaga(models.Model):
    # tempo integral, meio período, temporário
    # estágio, voluntário ou outros(?)
    nome = models.CharField(max_length=20)

    def __str__(self) -> str:
        return self.nome


class Skill(models.Model):
    nome = models.CharField(max_length=20)

    def __str__(self) -> str:
        return self.nome

    class Meta:
        ordering = ['nome']


class Dev(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    nome_completo = models.CharField(max_length=100)
    cidade = models.CharField(max_length=50)
    telefone = models.CharField(max_length=20)
    modelo_de_presenca = models.ManyToManyField(ModeloDePresenca)
    tipo_de_trabalho = models.ManyToManyField(TipoDeTrabalho)
    tipo_de_vaga = models.ManyToManyField(TipoDeVaga)
    skills = models.ManyToManyField(Skill)

    def __str__(self) -> str:
        return self.nome_completo


class Recruiter(models.Model):
    #hiring needs
    #skills
    #company size: 1-10 11-50 51-250 251-10K 10K+
    #nome
    #email
    #telefone
    #site da empresa
    # hiring call
    pass


class Vaga(models.Model):
    #titulo
    #modelo, tipotrabalho, tipovaga
    #skills
    #descrição
    pass
