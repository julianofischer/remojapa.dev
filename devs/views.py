from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render
from .forms import DevForm, DeveloperForm
from devs.models import Dev
from django.contrib.auth.models import User
from django.db import transaction

# Create your views here.
@transaction.atomic
def signup(request):
    if request.method == 'GET':
        userform = UserCreationForm()
        #devform = DevForm()
        devform = DeveloperForm()
    else:
        userform = UserCreationForm(request.POST)
        devform = DeveloperForm(request.POST)
        if userform.is_valid() and devform.is_valid():
            user = userform.save()
            dev = devform.save(commit=False)
            dev.usuario = user
            dev.save()
            ''' add accepts a arbitrary number of arguments instead of a list
                converting queryset to list > unpacking using start operator
            '''
            dev.modelo_de_presenca.add(*list(devform.cleaned_data['modelo_de_presenca']))
            dev.tipo_de_trabalho.add(*list(devform.cleaned_data['tipo_de_trabalho']))
            dev.tipo_de_vaga.add(*list(devform.cleaned_data['tipo_de_vaga']))
            dev.skills.add(*list(devform.cleaned_data['skills']))
            dev.save()
            '''dev = Dev()
            dev.usuario = user
            dev.cidade = devform.cleaned_data['cidade']
            dev.telefone = devform.cleaned_data['telefone']
            dev.save()
            dev.modelo_de_presenca.sets = devform.cleaned_data['modelo_de_presenca']
            dev.tipo_de_trabalho.sets = devform.cleaned_data['tipo_de_trabalho']
            dev.tipo_de_vaga.sets = devform.cleaned_data['tipo_de_vaga']
            dev.skills.sets = devform.cleaned_data['skills']
            dev.save()'''

    return render(request, 'devs/signup.html', {'userform': userform, 'devform': devform})
