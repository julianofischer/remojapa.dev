from django.contrib import admin
from devs.models import Dev, TipoDeTrabalho, TipoDeVaga, ModeloDePresenca, Skill
# Register your models here.

admin.site.register(Dev)
admin.site.register(TipoDeTrabalho)
admin.site.register(TipoDeVaga)
admin.site.register(ModeloDePresenca)
admin.site.register(Skill)